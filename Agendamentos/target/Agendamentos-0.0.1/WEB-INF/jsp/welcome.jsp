<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en" ng-app="app">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>titulo</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="js/app.js"></script>

<script src="js/mainController.js"></script>
<script>
	/*<![CDATA[*/

	/*]]>*/
</script>
</head>
<body ng-controller="mainController">


	<button id="btnFinalizarPedido" class="ui-button ui-widget ui-corner-all">{{nome}}</button>
	<br />
	<button id="btnTeste">Este botao realiza um ajax e carrega o select</button>


</body>
</html>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>



<!DOCTYPE html>
<html lang="en">
<head>
<title>AngularJS Blog</title>

<link rel="icon" href="/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<link rel="stylesheet" href="resources/css/bootstrap.min.css" media="screen" />
<link rel="stylesheet" href="resources/css/blog.css" media="screen" />

<script src="resources/js/libs/jquery-3.2.1.min.js"></script>
<script src="resources/js/libs/bootstrap.min.js"></script>
<script src="resources/js/libs/angular.min.js"></script>
<script src="resources/js/libs/angular-route.min.js"></script>
<script src="resources/js/libs/angular-resource.min.js"></script>
<script src="resources/js/libs/angular-cookies.min.js"></script>


<script src="resources/js/loginApp.js"></script>
<script src="resources/js/services.js"></script>

</head>
<body ng-app="loginApp">


	<div class="container loginForm" ng-controller="loginController">

		<div class="page-header">
			<h1>Login</h1>
		</div>

		<div class="row" >
			<form name='f' action="perform_login" method='POST' novalidate="novalidate">
				<div class="form-group">
					<label for="username">username:</label> 
					<input ng-required="true" ng-minlength="4" ng-model="username"  type="text" class="form-control " autocomplete="off" id="username" name="username" placeholder="Enter username">
				</div>
				
				<!--  o ng-hide usa pra esconder inicialmente o campo que aparece por um breve periodo durante o relaoad -->
				<div class="form-group ng-hide" ng-show="f.username.$error.minlength"">
					<label for="username">Usuario deve ter ao menos 4 caracteres!</label> 
				</div>				
   
				
				
				<div class="form-group">
					<label for="password">Password:</label> 
					<input ng-required="true" ng-minlength="4"  ng-model="password" type="password" autocomplete="off" class="form-control" id="password" name="password" value="">
				</div>
				<div class="checkbox">
					<label><input type="checkbox" ng-disabled="!f.$valid"> Remember me</label>
				</div>

				<button type="submit" class="btn btn-primary"  ng-disabled="!f.$valid">Login</button>
			</form>
		</div>

				<div class="form-group">
					<div class="alert alert-danger" ng-show="loginError && !f.$dirty">
						<strong>Usuario ou Password incorretos.</strong> 
					</div>
				</div>
				

	</div>
</body>
</html>





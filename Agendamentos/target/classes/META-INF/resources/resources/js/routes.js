app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider
	
	.when('/', {
		templateUrl: 'resources/partials/home.html',
		controller: 'homeController'

	})
	.when('/lancamentos', {
		templateUrl: 'resources/partials/lancamentos.html',
		controller: 'lancamentosController'

	});
	
	$locationProvider.html5Mode(false).hashPrefix('!');
}]);
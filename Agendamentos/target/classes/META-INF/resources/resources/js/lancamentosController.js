app.controller("lancamentosController",function($scope, $window, $http){

	
	$scope.categorias=[
		
		{
			id__:1,
			name:'Filmes'
		},
		{
			id__:2,
			name:'Animacoes'
		},		
		{
			id__:3,
			name:'Serie'
		}
		
	];

	$scope.tags=[
		
		{id__:1,label:'Action'},
		{id__:2,label:'Adventure'},
		{id__:11,label:'Film Noir'},
		{id__:12,label:'History'},	
		{id__:13,label:'Horror'},		
		{id__:6,label:'Crime'},
		{id__:7,label:'Documentary'},
		{id__:8,label:'Drama'},
		{id__:3,label:'Animation'},
		{id__:4,label:'Biography'},
		{id__:5,label:'Comedy'},
		{id__:9,label:'Family'},
		{id__:10,label:'Fantasy'},
		{id__:17,label:'Music'},
		{id__:18,label:'Musical'},
		{id__:16,label:'Mystery'},
		{id__:14,label:'Romance'},
		{id__:15,label:'Sci-Fi'},	
		{id__:19,label:'Short'},
		{id__:20,label:'Sport'},
		{id__:24,label:'Superhero'},
		{id__:23,label:'Thriller'},
		{id__:22,label:'War'},
		{id__:21,label:'Western'},
		{id__:30,label:'Porn'},
		{id__:31,label:'XXX'}
		
	];
	
	$scope.produtos =[
		
		{
			id__:3,
			dataEntrada: new Date(2018, 1, 19, 20, 6, 0, 0),
			titulo: 'DC Showcase: Superman/Shazam!: The Return of Black Adam',
			subtitulo: 'Superman e Shazam: O Retorno de Adão Negro',
			preco:98.25,
			iconeP: '/resources/img/bannerP/img0003.jpg',
			iconeG: '/resources/img/bannerG/img0003.jpg',
			categoria: 'animacoes',
			imdb:'http://www.imdb.com/title/tt1701223/releaseinfo',
			descricao:'O desenho mostra Clark Kent entrevistando o garoto Billy Batson, que secretamente é o Capitão Marvel. Juntos, Superman e Capitão Marvel terão de enfrentar o vilão assassino Adão Negro.'
		},
		
		{
			id__:1,
			dataEntrada: new Date(2018, 1, 20, 20, 7, 0, 0),
			titulo: 'Necessary Evil: Super-Villains of DC Comics',
			subtitulo: 'Necessary Evil: Super-Villains of DC Comics',
			preco:50.5,
			iconeP: '/resources/img/bannerP/img0001.jpg',
			iconeG: '/resources/img/bannerG/img0001.jpg',
			categoria: 'animacoes',
			imdb:'http://www.imdb.com/title/tt3033478',
			descricao:'"Necessary Evil: Super-Villains of DC Comics" é um documentário detalhado que conta com a participação de vários diretores/roteiristas consagrados, contando as mais variadas histórias sobre os super-vilões da DC Comics. Coringa, Lex Luthor, Sinestro e Darkseid estão entre eles.'
		},	
		
		{
			id__:2,
			dataEntrada: new Date(2017, 1, 20, 20, 7, 0, 0),
			titulo: 'Superman/Doomsday',
			subtitulo: 'Superman Doomsday',
			preco:60.95,
			iconeP: '/resources/img/bannerP/img0002.jpg',
			iconeG: '/resources/img/bannerG/img0002.jpg',
			categoria: 'animacoes',
			imdb:'http://i.legendas.tv/poster/214x317/c8/f8/tt1701223.jpg',
			descricao:'Adaptação da saga A Morte do Superman, tendo também alguns elementos de O Retorno do Superman. Ele é o primeiro de uma nova linha de animações em longa metragem de personagens da DC Comics, com lançamento direto em DVD. A intenção desta nova linha é adaptar histórias marcantes dos quadrinhos contando, sempre que possível, com a colaboração dos próprios criadores das versões em HQ. Cada longa metragem será totalmente isolado dos demais, bem como das séries animadas dos personagens.'
		},			
		
		{
			id__:4,
			dataEntrada: new Date(2017, 12, 15, 12, 7, 0, 0),
			titulo: 'Blade Runner 2049',
			subtitulo: 'Blade Runner 2049',
			preco:100.95,
			iconeP: '/resources/img/bannerP/img0004.jpg',
			iconeG: '/resources/img/bannerG/img0004.jpg',
			categoria: 'filmes',
			imdb:'http://i.legendas.tv/poster/214x317/c8/f8/tt17012240.jpg',
			descricao:'Trinta anos após os acontecimentos do primeiro filme, a humanidade está novamente ameaçada, e dessa vez o perigo pode ser ainda maior. Isso porque o novato oficial K (Ryan Gosling), desenterrou um terrível segredo que tem o potencial de mergulhar a sociedade no completo caos. A descoberta acaba levando-o a uma busca frenética por Rick Deckard (Harrison Ford), desaparecido há 30 anos.'
		}		
		
		
		
		
	];
	

	
	
	

	
	
});
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<link rel="stylesheet" href="resources/css/bootstrap.min.css" media="screen"/>
<link rel="stylesheet" href="resources/css/blog.css" media="screen"/>

<script src="resources/js/libs/jquery-3.2.1.min.js"></script>
<script src="resources/js/libs/bootstrap.min.js"></script>
<script src="resources/js/libs/angular.min.js"></script>
<script src="resources/js/libs/angular-route.min.js"></script>
<script src="resources/js/libs/angular-resource.min.js"></script>
<script src="resources/js/libs/angular-cookies.min.js"></script>

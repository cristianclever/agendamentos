<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!-- 
https://www.w3schools.com/angular/angular_select.asp
 -->
 
 
<!-- chapter5/index.html -->
<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
<title>I LOVE TV</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="resources/css/main.css" media="screen" />
<%@ include file="./../include/scriptHeaders.jsp"%>

<script src="resources/js/app.js"></script>
<script src="resources/js/lancamentosController.js"></script>
<script src="resources/js/homeController.js"></script>
<script src="resources/js/routes.js"></script>

</head>
<body class="site has-navbar-fixed-top" >

	<header role="banner" id="top" class="site-header">

		<!-- Main navigation -->
		<nav class="navbar navbar-fixed-top navbar-primary">
			<div class="container">

				<!-- Brand and menu toggle -->
				<div class="navbar-header">


					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainMenu">
						<span class="sr-only">Menu</span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span>
					</button>

					<a href="/scheduler" class="navbar-brand offset-right-large"> 
						<span class="visible-xs visible-md visible-lg">Sistema de Agendamentos</span>
					</a>
				</div>

				<!-- Collapsible menu -->
				<div class="collapse navbar-collapse" id="mainMenu">

					<ul class="nav navbar-nav">
						<li class="active">
							<a href="/orders/new"> <span class="glyphicon glyphicon-inbox offset-right" aria-hidden="true"></span> <span class="hidden-sm">Orders</span>
							</a>
						</li>
						<li><a href="/pages"> <span class="glyphicon glyphicon-align-left offset-right" aria-hidden="true"></span> <span class="hidden-sm">Pages</span>
						</a></li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li class="navbar-icon">
							<a href="../scheduler"  title="Home"> 
								<span class="glyphicon glyphicon-home" aria-hidden="true"></span> 
								<span class="visible-xs">Home</span>
							</a>
						</li>
						<li class="navbar-icon">
							<a href="#" title="Settings"> 
								<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> 
								<span class="visible-xs">Settings</span>
							</a>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" title="My account"> 
							<span class="glyphicon glyphicon-user offset-right" aria-hidden="true"></span> 
							<span class="visible-xs">My account</span> 
							<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
						</a>
							<ul class="dropdown-menu">
								<li class="dropdown-header">${nome}</li>
								<li class="divider"></li>
								<li>
									<a href="/"> 
									<span class="glyphicon glyphicon-off offset-right icon-shift-down" aria-hidden="true"></span> 
									Sign out</a>
								</li>
							</ul></li>
						<!-- .dropdown -->
					</ul>
					<!-- .nav -->

				</div>
				<!-- .collapsible -->

			</div>
			<!-- .container -->
		</nav>

	</header>


	<!--  Conteudo Principal INICIO -->
	<div ng-include src="'resources/partials/menu.html'"></div>
	<!--  Conteudo Principal FIM -->

	<main role="main" class="site-content">
	<div ng-view></div>
	</main>

	<footer role="contentinfo" class="footer site-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-9">

					© VisionApps, s.r.o. 2016 <span class="footer-divider">|</span> Built on <a href="http://www.bootstrap-ui.com" target="_blank">Bootstrap UI</a>

				</div>
				<div class="col-md-3">

					<a href="http://www.visionapps.cz" target="_blank" class="footer-author" title="Created by VisionApps"> <img src="/images/visionapps.svg?v=1203031868" width="90" height="16" alt="VisionApps" />
					</a>

				</div>
			</div>
			<!-- .row -->
		</div>
		<!-- .container -->
	</footer>


</body>
</html>


<!-- 
http://demo.bootstrap-ui.com/orders/canceled
Pagina para exibicao dos agendamentos:

Listagem dos recursos disponíveis (em formato de carrosel) , cada item tem um check box.
- Range de datas
- Lista 'live' com os resultados das pesquisas. cada item pode ser visualizado com detalhes


- Area destinada ao cadastro de novos itens






 -->
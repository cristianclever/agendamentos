package controllers;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.agendamentos.bo.CommonBO;


@Controller
@Scope("session")
@RequestMapping("/scheduler")
public class MainController {

	@Autowired
	private EntityManager em;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private CommonBO commomBO;
/*	
	
   @PreAuthorize("hasRole('ADMIN')")
    void updateUser(User user);
     
    @PreAuthorize("hasRole('ADMIN') AND hasRole('DBA')")
    void deleteUser(int id);
	
*/	
	
	@RequestMapping( method = RequestMethod.GET)

	//  @Secured({ "ROLE_DBA", "ROLE_ADMIN" })
	
	public ModelAndView index() {
		
		int x = 0 ;
		
		commomBO.makeAdminOPeration();
		
		commomBO.makeUserOPeration();
		
		UserDetails userDetails = commomBO.getUserDetails();
		

		
		 	
		
		
		ModelAndView mv = new ModelAndView("main");
		//mv.getModelMap().addAttribute("nome", userDetails.getUsername());
		mv.addObject("nome", "Cristian Clever de Oliveira");

		return mv;
	}
	
	/*
	  @RequestMapping(value = { "/edit-user-{id}" }, method = RequestMethod.GET)
	    public String editUser(@PathVariable int id, ModelMap model) {
	        User user  = service.findById(id);
	        model.addAttribute("user", user);
	        model.addAttribute("edit", true);
	        return "registration";
	    }
	    */ 	
	
	@Autowired
	HttpServletRequest request;
	
	@RequestMapping(path="/usercredentials" , method = RequestMethod.GET)
	@ResponseBody
	public UserDetails getUserCredentials(){
		
		
		String terst = request.getParameter("test");
		UserDetails userDetails = commomBO.getUserDetails();
		return userDetails;
	}
	
	
	
}

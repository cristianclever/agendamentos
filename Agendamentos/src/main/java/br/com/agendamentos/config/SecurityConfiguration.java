package br.com.agendamentos.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/*
 * Abaixo um exemplo das configuracoes utilizadas para restricao via jsp
 * https://docs.spring.io/spring-security/site/migrate/current/3-to-4/html5/migrate-3-to-4-xml.html#m3to4-deprecations-taglibs
 * 
 * http://websystique.com/spring-security/spring-security-4-secure-view-layer-using-taglibs/
 */



@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=false,prePostEnabled=true ) // prePostEnabled ´´e que permite a validacao dos métodos usando as anotacoes
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	
/*	
	
	
	
	
  private static final String PERMISSOES_POR_USUARIO = "SELECT u.login, p.nome FROM usuario_permissao up"
            + " JOIN usuario u ON u.id = up.usuarios_id"
            + " JOIN permissao p ON p.id = up.permissoes_id"
            + " WHERE u.login = ?";
	
  private static final String PERMISSOES_POR_GRUPO = "SELECT g.id, g.nome, p.nome FROM grupo_permissao gp"
            + " JOIN grupo g ON g.id = gp.grupos_id"
            + " JOIN permissao p ON p.id = gp.permissoes_id"
            + " JOIN usuario_grupo ug ON ug.grupos_id = g.id"
            + " JOIN usuario u ON u.id = ug.usuarios_id"
            + " WHERE u.login = ?";
		
  @Autowired
  private DataSource dataSource;
 
 
  ...
 
	
  @Override
  protected void configure(AuthenticationManagerBuilder builder) throws Exception {
    builder
        .jdbcAuthentication()
        .dataSource(dataSource)
        .passwordEncoder(new BCryptPasswordEncoder())
        .usersByUsernameQuery(USUARIO_POR_LOGIN)
        .authoritiesByUsernameQuery(PERMISSOES_POR_USUARIO)
        .groupAuthoritiesByUsername(PERMISSOES_POR_GRUPO)
        .rolePrefix("ROLE_");
  }	
	

*/	
	

	
/*	
protected void configure(HttpSecurity http) throws Exception {
  http
    .authorizeRequests()
        .anyRequest().authenticated()
    .and()
    .formLogin()
        // Aqui dizemos que temos uma página customizada.
        .loginPage("/login") 
        // Mesmo sendo a página de login, precisamos avisar
        // ao Spring Security para liberar o acesso a ela.
        .permitAll(); 
}	
*/	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		
		auth.inMemoryAuthentication().withUser("ram").password("ram123").roles("ADMIN");
		auth.inMemoryAuthentication().withUser("ravan").password("ravan123").roles("USER");
		auth.inMemoryAuthentication().withUser("cris").password("cris123").roles("USER");
		  
		  
		/*
		auth.
		jdbcAuthentication()
			.usersByUsernameQuery(usersQuery)
			.authoritiesByUsernameQuery(rolesQuery)
			.dataSource(dataSource)
			.passwordEncoder(bCryptPasswordEncoder);		
		
		*/
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		//https://www.journaldev.com/8718/spring-4-security-mvc-login-logout-example
		//Aqui uotimo exemplo
		
		
		//Basicamente, começa primeiro com as restriçoes, e depois libera o resto
		
		http.csrf().disable()
		.authorizeRequests()
		

		.antMatchers("/scheduler/**").hasAnyRole("ADMIN","USER")
		.antMatchers("/autenticado/**").hasAnyRole("ADMIN","USER")
		.antMatchers("/user/**").hasAnyRole("USER")
		.antMatchers("/admin/**").hasAnyRole("ADMIN")
		.anyRequest().permitAll()
		.and()
		.formLogin().loginPage("/login.jsp")
		.loginProcessingUrl("/perform_login")
		.defaultSuccessUrl("/scheduler") //pagina principal da aplicacao
		.failureUrl("/login.jsp?error")
		.usernameParameter("username").passwordParameter("password")	
		
		.and()
		.logout().logoutSuccessUrl("/login.jsp?logout&x=3"); 
		
		
		/*
		//super.configure(http);
		//http.authorizeRequests().antMatchers("/info/**").hasAnyRole("ADMIN","USER").and().formLogin();		
		http.authorizeRequests()
		/*.antMatchers("/all/**").hasAnyRole("ADMIN","USER")
		.antMatchers("/user/**").hasAnyRole("USER")
		.antMatchers("/admin/**").hasAnyRole("ADMIN")
		.antMatchers("/**").permitAll()
		.and().formLogin();
		*/
		
		/*

        http
            .authorizeRequests()
                .antMatchers("/", "/home").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
            .logout()
                .permitAll();

		
		*/
		
		
		/*
		http.authorizeRequests()
			.antMatchers("/homePage").access("hasRole('ROLE_USER')")
			.and()
				.formLogin().loginPage("/loginPage")
				.defaultSuccessUrl("/homePage")
				.failureUrl("/loginPage?error")
				.usernameParameter("username").passwordParameter("password")				
			.and()
				.logout().logoutSuccessUrl("/loginPage?logout"); 
		*/
		
/*		
		
http.csrf().disable()
		    .authorizeRequests()
		    .antMatchers("/user/**").hasAnyRole("ADMIN","USER")
		    .and().httpBasic().realmName("MY APP REALM")
		    .authenticationEntryPoint(myAppBasicAuthenticationEntryPoint);		
		
*/	
		
	
	}

	
	
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		super.configure(web);
	}

}

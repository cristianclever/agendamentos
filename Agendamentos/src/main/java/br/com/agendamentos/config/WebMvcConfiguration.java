package br.com.agendamentos.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@EnableAspectJAutoProxy
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {
	
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login"). setViewName("login");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// TODO Auto-generated method stub
		super.addResourceHandlers(registry);
		/*
		
        registry.setOrder(Integer.MIN_VALUE);
        registry.addResourceHandler("/favicon.ico")
            .addResourceLocations("/")
            .setCachePeriod(0);*/
	}
    
    

    
    
}

package br.com.agendamentos.bo;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class CommonBO {
	
	// @PreAuthorize("hasRole('ADMIN')")
	public boolean makeAdminOPeration(){
		System.out.println("CommonBO.makeAdminOPeration()");
		return true;
	}
	 //@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public boolean makeUserOPeration(){
		System.out.println("CommonBO.makeUserOPeration()");
		return true;
	}	
	
	
	
	public UserDetails getUserDetails(){
		
		
		

		UserDetails userDetails = null;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
		        // userDetails = auth.getPrincipal()
			userDetails =  (UserDetails)auth.getPrincipal();	
		}
		
		return userDetails;
		
	}
	
	

}

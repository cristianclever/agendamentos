package br.com.agendamentos.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import br.com.agendamentos.bo.CommonBO;
import br.com.agendamentos.config.PersistenceJPAConfig;
import controllers.MainController;


@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableAutoConfiguration
@ComponentScan(basePackageClasses =  {PersistenceJPAConfig.class, MainController.class , CommonBO.class})
@SpringBootApplication
public class AngendamentosMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(AngendamentosMainApplication.class, args);
    }
    
}

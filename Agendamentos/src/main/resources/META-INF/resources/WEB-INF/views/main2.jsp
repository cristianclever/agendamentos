<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html ng-app="app">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width" />
    
    <title>Posts</title>
    

<%@ include file= "./../include/scriptHeaders.jsp" %>

<style type="text/css">

/*
.ng-valid       {     color: green; }
.ng-invalid     {     color: red; }
*/

.has-error{background-color: #fff0f0}


</style>

</head>
<body ng-controller="cadastrarController" id="body">

<my-directive name="test"></my-directive>


<div class="container">

  <div class="page-header">
    <h2>Novo Cadastro de Usu�rio</h2>      
  </div>

	


	<form class="form-horizontal" name="mform" novalidate>

	    <div class="form-group">
	      	<label class="" for="email" class="">Nome:</label>

		    <input type="text" ng-class="{ 'has-error' : mform.inputNome.$invalid && !mform.inputNome.$pristine }" autocomplete="off" class="form-control" id="input-nome" name="inputNome" ng-minlength="1" ng-model="usuario.nome"  ng-required="true"/> 

	    </div>
		
	    <div class="form-group">
			<label for="input-login">Login:</label> 
			<input type="text" autocomplete="off"  class="form-control" ng-maxlength="3" id="input-login" name="inputlogin" ng-model="usuario.login" /> 
	    </div>
	    <!-- 		
		<h1>{{form.inputlogin.$valid}}</h1>
 		-->
		 <div class="form-group">
			<label for="input-senha">Senha:</label> 
			<input type="password"   autocomplete="off"  class="form-control" id="input-senha"  ng-model="usuario.senha" /> 
		 </div>
		
		

		 
	    <div class="checkbox">
	      <label><input type="checkbox"  ng-model="usuario.ativo">Ativo:</label>
	    </div>		 
		 


		 <div class="form-group">
  			<button type="button" class="btn btn-primary" ng-click="cancelar()">Cancelar</button>
  			<button type="button" class="btn btn-primary" ng-click="salvarUsuario()" ng-disabled="mform.$invalid">Salvar</button>
		</div>


	</form>
	
	<!-- 

	Acessando o scope a partir do console.
	--------------------------------------
	var scope = angular.element(document.getElementById('body')).scope(); -- acessa o elemento do scopo, no caso o controller
	scope.salvar() // -- acessa uma funcao do controller angular.
	
	$scope.clients = [
		    { id: 1, name: 'John', age: 25, percentage: 0.3, sex:'M' },
		    { id: 2, name: 'Jane', age: 39, percentage: 0.18 , sex:'F' },
		    { id: 3, name: 'Jude', age: 51, percentage: 0.54 , sex:'F' },
		    { id: 4, name: 'James', age: 18, percentage: 0.32 , sex:'M' },
		    { id: 5, name: 'Javier', age: 47, percentage: 0.14 , sex:'M' }
		  ];	
		
		
	=============================================================================	
	One way data Bind
	-----------------
	
	O oneway data bind eh um mecanismo de performance.
	use em locais que n�o requerem altera��es 
	ex:
	<div >
		<div ng-repeat="client in ::clients">
			{{::client.name}}
		</div>
	</div>			
		
		
		  
	=======================================
	Um exemplo do filter aplicado
	-----------------------------
	
	<div >
		<div ng-repeat="client in clients|filter:{sex:'M'}">
			{{client.name}}  + {{$index}} Index � o indice, ha outras como $first
		</div>
	</div>
	
	
	=============================================================================
	Usando ng-if ao inves de um filter
	----------------------------------
	<div >
		<div ng-repeat="client in clients">
			<div ng-if="client.sex=='F'" style="background-color: #0F3">
				{{client.name}} 
			</div>
			<div ng-if="client.sex=='M'" style="background-color: #BF9">
				{{client.name}} 
			</div>			
		</div>
	</div>		
	 -->


 <!-- Modal -->
  <div class="modal fade" id="modalError" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Aviso</h4>
        </div>
        <div class="modal-body">
          <p><span id='spanModalError'></span></p>
        </div>
      </div>
    </div>
  </div>
</div>
	
		
 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Aviso</h4>
        </div>
        <div class="modal-body">
          <p>Registro salvo com sucesso.</p>
        </div>
      </div>
    </div>
  </div>
</div>
	
</div>


</body>
</html>
var app = angular.module("loginApp",[]);

app.config( [ '$locationProvider', function( $locationProvider ) {
	   // In order to get the query string from the
	   // $location object, it must be in HTML5 mode.
		$locationProvider.html5Mode({
			  enabled: true,
			  requireBase: false
		});
	
	}]);

app.controller("loginController", ['$scope','$window', '$http','$location', function($scope, $window, $http,$location){

	$scope.loginError = false;
	
	$scope.username="";
	$scope.password="";
	
	
	(function(angular) {
		//init
		initialize();
		})(window.angular);

	function initialize(){
		

		if ( $location.search().hasOwnProperty( 'error' ) ) {
			$scope.loginError = true;
		}
		
		
	}
	
	
	
	
	
	
	
}]);